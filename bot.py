import logging
import os
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, CallbackContext
from dotenv import load_dotenv
from pathlib import Path
import gitlab

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)
BOT_TOKEN = os.getenv("BOT_TOKEN")
CHAT_ID = os.getenv("CHAT_ID")
gl = gitlab.Gitlab("https://gitlab.com", private_token=os.getenv("GITLAB_TOKEN"))

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)
logger = logging.getLogger(__name__)

def start(update: Update, context: CallbackContext) -> None:
    if str(update.message.chat_id) == str(CHAT_ID):
        keyboard = [
            [
                InlineKeyboardButton("Pumped, Energized", callback_data='1'),
                InlineKeyboardButton("Happy, Excited", callback_data='2'),
            ],
            [
                InlineKeyboardButton("Good, Alright", callback_data='3'),
                InlineKeyboardButton("Down, Worried", callback_data='4'),
            ],
            [
                InlineKeyboardButton("Sad, Unhappy", callback_data='5'),
                InlineKeyboardButton("Miserable, Nervous", callback_data='6'),
            ],
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text('😎 How are you feeling today?', reply_markup=reply_markup)
    else:
        update.message.reply_text('Get our from here 🚪')

def button(update: Update, context: CallbackContext) -> None:
    query = update.callback_query
    query.answer()
    query.delete_message()
    query.from_user.send_message("Writing in my book 📗")
    project = gl.projects.get('23590604', lazy=True)
    if query.data == '1':
        query.from_user.send_message("Got it! It's marked in the books 📚")
        mood = "🔥 Pumped"
    elif query.data == '2':
        query.from_user.send_message("Got it! It's marked in the books 📚")
        mood = "😄 Happy"
    elif query.data == '3':
        query.from_user.send_message("Got it! It's marked in the books 📚")
        mood = "😇 Good"
    elif query.data == '4':
        query.from_user.send_message("Sending hugs 🤗")
        mood = "😔 Down"
    elif query.data == '5':
        query.from_user.send_message("Sending hugs 🤗")
        mood = "😢 Unhappy"
    elif query.data == '6':
        query.from_user.send_message("Sending hugs 🤗")
        mood = "😭 Nervous"
    issue = project.issues.create({'title': mood})

def main():
    updater = Updater(BOT_TOKEN, use_context=True)
    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CallbackQueryHandler(button))
    updater.start_polling()
    updater.idle()

main()
